1. Сначала прописать в bashrc
```
echo "export export TURTLEBOT3_MODEL=burger" >> ~/.bashrc
echo "export export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/opt/ros/galactic/share/turtlebot3_gazebo/models" >> ~/.bashrc
```

2. Запускать мир с turtlebot3 и лабиринтом:
```
ros2 launch starline_2022 spawn_my_turtlebot3.launch.py
```
